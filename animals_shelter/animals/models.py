from django.db import models
from django.core.validators import MaxValueValidator, RegexValidator
from django.core.exceptions import ValidationError


def validate_date(value):
    year = value.year
    if year < 1900 or year > 2023:
        raise ValidationError('Введіть коректний рік народження.')


def validate_name(value):
    if not value.isalpha():
        raise ValidationError('Кличка повинна містити тільки букви')


class Animal(models.Model):
    ANIMAL_TYPES = [
        ('Кіт', 'Кіт'),
        ('Пес', 'Пес'),
        ('Птичка', 'Птичка'),
        ('Єнот', 'Єнот'),
        ('Черепаха', 'Черепаха'),

    ]
    entry_date = models.DateTimeField(auto_now=True)
    pet_type = models.CharField(max_length=20, verbose_name='Тип тваринки', choices=ANIMAL_TYPES)
    pet_name = models.CharField(validators=[validate_name], max_length=25, verbose_name='Кличка')
    pet_birthday = models.DateField(validators=[validate_date], verbose_name='День народження')
    pet_age = models.PositiveIntegerField(
        validators=[MaxValueValidator(150, message='Введіть коректний вік в повних роках')], verbose_name='Вік')
    pet_height = models.PositiveIntegerField(
        validators=[MaxValueValidator(150, message='Введіть коректний зріст в см')], verbose_name='Зріст')
    pet_weight = models.DecimalField(max_digits=7, decimal_places=2,
                                     validators=[MaxValueValidator(150, message='Введіть коректну вагу в кг')],
                                     verbose_name='Вага')
    pet_passport = models.CharField(
        validators=[RegexValidator(r'[A-Z]{2}\d{6}', message='Коректний формат: АА123456')],
        max_length=8, blank=True, null=True, verbose_name='Паспорт')

    def __str__(self):
        return self.pet_type, self.pet_passport

    class Meta:
        verbose_name = 'Animal'
        verbose_name_plural = 'Animals'
