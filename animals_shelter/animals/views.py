from django.shortcuts import render, get_object_or_404, redirect
from .models import Animal
from .forms import AnimalForm

HOME_PAGE = 'animals:animals_list'


def animals_list(request):
    animals = Animal.objects.all()
    animal_type = request.GET.get('pet_type')
    sort_by = request.GET.get('sort_by', 'id')

    if animal_type:
        animals = Animal.objects.filter(pet_type=animal_type)

    if sort_by:
        animals = animals.order_by(sort_by)

    return render(request, 'animals/animals_list.html', {
        'animals': animals,
        'sort_by': sort_by
    })


def add_new_animal(request):
    if request.method == 'POST':
        form = AnimalForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(HOME_PAGE)
    else:
        form = AnimalForm()
    return render(request, 'animals/add_animal.html', {'form': form})


def animal_edit(request, animal_id):
    animal = get_object_or_404(Animal, id=animal_id)

    if request.method == 'POST':
        form = AnimalForm(request.POST, instance=animal)
        if form.is_valid():
            form.save()
            return redirect(HOME_PAGE)
    else:
        form = AnimalForm(instance=animal)

    return render(request, 'animals/animal_edit.html', {'form': form, 'animal': animal})


def animal_delete(request, animal_id):
    animal = get_object_or_404(Animal, id=animal_id)

    if request.method == 'POST':
        animal.delete()
        return redirect(HOME_PAGE)

    return render(request, 'animals/animal_delete.html', {'animal': animal})
