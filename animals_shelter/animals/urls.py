from django.urls import path
from . import views

app_name = 'animals'
urlpatterns = [
    path('', views.animals_list, name='animals_list'),
    path('add_animal/', views.add_new_animal, name='add_animal'),
    path('<int:animal_id>/animal_edit/', views.animal_edit, name='animal_edit'),
    path('<int:animal_id>/animal_delete/', views.animal_delete, name='animal_delete'),

]
