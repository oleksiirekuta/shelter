from django.apps import AppConfig


class AnimalsShelter(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'animals'
