from django import forms
from .models import Animal


class AnimalForm(forms.ModelForm):
    class Meta:
        model = Animal
        fields = '__all__'
        widgets = {
            'pet_birthday': forms.DateInput(attrs={'type': 'date'}),
        }

